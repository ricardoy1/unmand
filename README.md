As requested. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

I created a couple of tests for demo's sake. In Practice, more tests must
be added to ensure it's been covered.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


## Included Libraries
CSS FlexBox

## Task Details

Added basic unit tests.

Implemented a very simple UI.

The bonus task had an extra challenge since retrieving the comic information threw a CORS error. However, I found a work-around using a proxy.
