import React from 'react';
import { render, queryByAttribute } from '@testing-library/react';
import App from './App';

test('renders Game', () => {
  const getById = queryByAttribute.bind(null, 'data-test-id');
  const dom = render(<App />);
  const table = getById(dom.container, 'game');
  expect(table).toBeDefined();
});
