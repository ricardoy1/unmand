import React, {Component} from 'react';
import { Grid, Row } from 'react-flexbox-grid';
import { Button } from 'reactstrap';
import CellGame from './CellGame'
import { getRandomIndex } from '../lib';

export default class GridGame extends Component {

    constructor(props) {
        super(props);
        this.rows = props.rows;
        this.columns = props.columns;
    }

    getBlankState = () => ({
            items: (
                Array.from(
                    {length: this.rows * this.columns},
                    (_, index) => ({id: index, selected: false}))
            )
        })

    componentWillMount() {
        this.setState(this.getBlankState());
    }

    resetSquares() {
        this.setState(this.getBlankState());
    }

    activateCell = (id) => {
        this.setState({
            items: this.state.items.map((item, _) =>
              item.id === id ? { ...item, selected: true } : item,
           )
        })
    }

    isActive = (id) => {
        return this.state.items.find(item => item.id === id).selected;
    }

    areAllCellsSelected = () => this.state.items.filter(item => !item.selected).length === 0;

    buttonClickListener = () => {
        var inactive = this.state.items.filter(item => !item.selected);
        if(inactive.length === 0){
            this.resetSquares();
        } else {
            var randomIndex = getRandomIndex(inactive.length - 1);
            this.activateCell(inactive[randomIndex].id);
        }
    }

    getSequenceId = (column, row) => column*this.rows + row;

    render() { 
        return (
        <div className="container">
            <div className="row">
                <Grid fluid className="grid">
                    {[...new Array(this.rows)].map((_, row) => 
                        <Row key={row}>
                             {[...new Array(this.columns)].map((_, column) => 
                                 <CellGame 
                                    key={this.getSequenceId(column, row)}
                                    isActive={this.isActive(this.getSequenceId(column, row))}>
                                 </CellGame>
                             )}
                        </Row>
                    )}
                </Grid>
            </div>

            <Button
                onClick={this.buttonClickListener}
                className="primary">
                { this.areAllCellsSelected() ? "Reset" : "Play"}
            </Button>
        </div>
        );
    }
}