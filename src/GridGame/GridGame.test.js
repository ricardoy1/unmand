import React from 'react';
import { render } from '@testing-library/react';
import GridGame from './index';

test('renders cells', () => {
  const dom = render(<GridGame rows={2} columns={2} />);
  const cells = dom.container.querySelectorAll('.cell');
  expect(cells.length).toBe(4);
});
