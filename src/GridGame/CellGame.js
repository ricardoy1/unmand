import React, {Component} from 'react';
import { Col } from 'react-flexbox-grid';


export default class CellGame extends Component {
    constructor(props) {
        super(props);
        this.isActive = props.isActive;
      }

    render = () => (
            <Col className={"cell " + (this.props.isActive ? "selected" : "")}></Col>
        );
}