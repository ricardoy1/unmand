import React from 'react';
import './App.css';
import GridGame from './GridGame'
import XKCD from './XKCD';

function App() {
  return (    
    <div className="App">
      <GridGame data-test-id="game" rows={4} columns={4} />

      <XKCD/>
    </div>
  );
}

export default App;
