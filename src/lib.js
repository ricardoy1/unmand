
function getRandom(from, to) {
    var min = Math.ceil(from);
    var max = Math.floor(to);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

function getRandomIndex (top) {
    return getRandom(0, top);
};

export { getRandomIndex, getRandom };