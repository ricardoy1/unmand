import React, {Component} from 'react';
import { Button } from 'reactstrap';
import { getRandom } from '../lib';
import fetchComic from './fetchComic'

export default class XKCD extends Component {

    startCall = () => this.setState(Object.assign(this.state || {}, {
        calling: true
    }, {}));

    stopCall = () => this.setState(Object.assign(this.state || {}, {
        calling: false
    }, {}));

    fetchComic = () =>{
        const randomComicId = getRandom(1, 2000);
        this.startCall();
        fetchComic(randomComicId)
        .then(joke => {
            this.stopCall();
            this.setState(Object.assign(this.state || {}, {
                altText: joke.alt
            }, {}));
        })
        .catch(_ => this.stopCall());
      }

    componentWillMount() {
        this.fetchComic();
    }

    render() { 
        var message = <p></p>;
        if(this.state) {
            message = <p>{this.state.altText}</p>;
        }
        return (
        <div className="container">
            <div className="row">
                <h3>{message}</h3>
            </div>
            <div className="row">
                <Button
                    disabled={this.state.calling}
                    onClick={this.fetchComic}
                    className="primary">
                    Another Joke!
                </Button>
            </div>
        </div>
        );
    }
}