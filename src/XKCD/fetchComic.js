const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';

function fetchComic(id){
    const URL = `https://xkcd.com/${id}/info.0.json`;
    return fetch(PROXY_URL+URL).then(response => response.json());
  }

export default fetchComic;